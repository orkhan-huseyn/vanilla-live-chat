const chatContainer = document.getElementById('chatContainer');
const messageForm = document.getElementById('messageForm');
const textarea = document.getElementById('textarea');
const downButton = document.getElementById('downButton');

const socket = new WebSocket('ws://10.50.92.54:8080');

chatContainer.addEventListener('scroll', function () {
  if (
    chatContainer.scrollTop + chatContainer.clientHeight <
    chatContainer.scrollHeight
  ) {
    downButton.style.display = 'block';
  } else {
    downButton.style.display = 'none';
  }
});

downButton.addEventListener('click', function () {
  chatContainer.scrollTo({
    top: chatContainer.scrollHeight,
    behavior: 'smooth',
  });
});

messageForm.addEventListener('submit', function (event) {
  event.preventDefault();
  socket.send(textarea.value);
  textarea.value = '';
});

socket.addEventListener('message', function (event) {
  const msg = JSON.parse(event.data);
  const html = getMessageHTML(msg);
  chatContainer.insertAdjacentHTML('beforeend', html);

  //   chatContainer.scrollTop = chatContainer.scrollHeight;
});

function getMessageHTML(message) {
  const color = randomColor();

  if (message.type == 'join') {
    return `
        <div class="message">
            <span style="color: ${color}" class="uname">${message.userName}</span> joined chat.
        </div>`;
  }

  if (message.type == 'ban') {
    return `
        <div class="message">
            <span style="color: ${color}" class="uname">${message.userName}</span> banned.
        </div>`;
  }

  return `
    <div class="message">
        <span style="color: ${color}" class="uname">${message.userName}:</span> ${message.text}
    </div>`;
}

function randomColor() {
  const randomHex = Math.floor(Math.random() * 0xffffff).toString(16);
  return `#${randomHex}`;
}
